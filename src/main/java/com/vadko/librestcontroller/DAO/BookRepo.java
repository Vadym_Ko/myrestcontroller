package com.vadko.librestcontroller.DAO;

import com.vadko.librestcontroller.Model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BookRepo extends JpaRepository <Book,Long>{

    Book findById (long id);
    List<Book> findByTitleStartsWithIgnoreCase (String title);
    List<Book> findByAuthorStartsWithIgnoreCase (String author);
    List<Book> findByYearStartsWithIgnoreCase (String year);
    List<Book> findByGenreStartsWithIgnoreCase (String genre);
    List<Book> findByBookcaseNumberStartsWithIgnoreCase (String genre);

}
