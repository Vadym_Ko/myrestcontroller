package com.vadko.librestcontroller.Controller;

import com.vadko.librestcontroller.Model.Book;
import com.vadko.librestcontroller.Service.LibService;
import com.vadko.librestcontroller.Service.BookResourceAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 *  Controller. It works with GET, PUT, POST and DELETE queries. Data must be
 *  in JSON format.
 */

@RequestMapping("/api/v1/book")
@RestController
public class LibController {

    private final LibService libService;
    private final BookResourceAssembler assembler;

    /**
     * Library controller
     * @param libService object of LibService type
     * @param assembler  object of BookAssembler type uses to wrap Entities
     */
    @Autowired
    public LibController(LibService libService, BookResourceAssembler assembler) {

        this.libService = libService;
        this.assembler = assembler;
    }

    /**
     * Add book to database
     * POST query to "/api/v1/book"
     * @param newBook book with all fields filled
     */
    @PostMapping
    public ResponseEntity<?> addBook(@RequestBody Book newBook) throws URISyntaxException {

        Resource<Book> resource = assembler.toResource(libService.saveBook(newBook));

        return ResponseEntity
                .created(new URI(resource.getId().expand().getHref()))
                .body(resource);
    }

    /**
     * Return book from database by Id
     * GET query to "/api/v1/book/id/{id}"
     * @param id books id
     * @return book with specified Id
     */
    @GetMapping (path = "id/{id}")
    public Resource<Book> getBookById (@PathVariable("id") long id){

        return assembler.toResource(libService.findBookById(id));
    }

    /**
     * Return a filtered list of books with specified title
     * GET query to "/api/v1/book/title/{title}"
     * @param title some letters or a whole title
     * @return found list of books according to the given parameter
     */
    @GetMapping (path = "/title/{title}")
    public Resources<Resource<Book>> getBooksByTitle (@PathVariable("title") String title){

        List<Resource<Book>> books = libService.findBooksByTitle(title).stream()
                .map(assembler::toResource)
                .collect(Collectors.toList());

        return new Resources<> (books,
                linkTo(methodOn(LibController.class).getAllBooks()).withRel("for all items"));
        }

    /**
     * Return a filtered list of books with specified author
     * GET query to "/api/v1/book/author/{author}"
     * @param author some letters or a whole author name
     * @return found list of books according to the given parameter
     */
    @GetMapping (path = "/author/{author}")
    public Resources<Resource<Book>> getBooksByAuthor (@PathVariable("author") String author){

        List<Resource<Book>> books = libService.findBooksByAuthor(author).stream()
                .map(assembler::toResource)
                .collect(Collectors.toList());

        return new Resources<> (books,
                linkTo(methodOn(LibController.class).getAllBooks()).withRel("for all items"));
        }

    /**
     * Return a filtered list of books with specified year
     * GET query to "/api/v1/book/year/{year}"
     * @param year some letters or a whole year
     * @return found list of books according to the given parameter
     */
    @GetMapping (path = "/year/{year}")
    public Resources<Resource<Book>> getBooksByYear (@PathVariable("year") String year){

        List<Resource<Book>> books = libService.findBooksByYear(year).stream()
                .map(assembler::toResource)
                .collect(Collectors.toList());

        return new Resources<> (books,
                linkTo(methodOn(LibController.class).getAllBooks()).withRel("for all items"));
        }

    /**
     * Return a filtered list of books with specified genre
     * GET query to "/api/v1/book/genre/{genre}"
     * @param genre some letters or a whole genre
     * @return found list of books according to the given parameter
     */
    @GetMapping (path = "/genre/{genre}")
    public Resources<Resource<Book>> getBooksByGenre (@PathVariable("genre") String genre){

        List<Resource<Book>> books = libService.findBooksByGenre(genre).stream()
                .map(assembler::toResource)
                .collect(Collectors.toList());

        return new Resources<> (books,
                linkTo(methodOn(LibController.class).getAllBooks()).withRel("for all items"));
        }

    /**
     * Return a filtered list of books with specified bookcase number
     * GET query to "/api/v1/book/bn/{bn}"
     * @param bn some letters or a whole bookcase number
     * @return found list of books according to the given parameter
     */
    @GetMapping (path = "/bn/{bn}")
    public Resources<Resource<Book>> getBooksByBookcaseNumber (@PathVariable("bn") String bn){

        List<Resource<Book>> books = libService.findByBookBookcaseNumber(bn).stream()
                .map(assembler::toResource)
                .collect(Collectors.toList());

        return new Resources<> (books,
                linkTo(methodOn(LibController.class).getAllBooks()).withRel("for all items"));
        }

    /**
     * Return all of books from database
     * GET query to "/api/v1/book/"
     * @return all list of books from database
     */
    @GetMapping
    public Resources<Resource<Book>> getAllBooks (){

        List<Resource<Book>> books = libService.findAllBooks().stream()
                .map(assembler::toResource)
                .collect(Collectors.toList());

        return new Resources<> (books,
                linkTo(methodOn(LibController.class).getAllBooks()).withRel("for all items"));
    }

    /**
     * Delete a book with specified Id from database
     * DELETE query to "/api/v1/book/id/{id}"
     * @param id id of the book for delete
     */
    @DeleteMapping(path = "id/{id}")
    public ResponseEntity<?> deleteBookByID (@PathVariable("id") long id){

        try {
            libService.deleteBook(id);
            return ResponseEntity.noContent().build();
        }
        catch (EmptyResultDataAccessException e) {
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Update book in database
     * PUT query to "/api/v1/book/id/{id}"
     * @param changedBook changed book
     */
    @PutMapping(path = "id/{id}")
    public ResponseEntity<?> updateBookByID(@RequestBody Book changedBook, @PathVariable("id") long id)
            throws URISyntaxException{

        try {
            Resource<Book> resource = assembler.toResource(libService.updateBook(changedBook,id));
            return ResponseEntity
                    .created(new URI(resource.getId().expand().getHref()))
                    .body(resource);
        } catch (NullPointerException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
