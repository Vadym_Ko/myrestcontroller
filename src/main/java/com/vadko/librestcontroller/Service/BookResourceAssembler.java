package com.vadko.librestcontroller.Service;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import com.vadko.librestcontroller.Controller.LibController;
import com.vadko.librestcontroller.Model.Book;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

/**
 * Class for converting Books to Resource with hipermedia links.
 */

@Component
public class BookResourceAssembler implements ResourceAssembler<Book,Resource<Book>> {

    /**
     * Method for converting Books to Resource with hipermedia links.
     * @param book Book for converting
     * @return Resource with hipermedia links
     */
    @Override
    public Resource<Book> toResource (Book book){

        return new Resource<Book>(book,
                linkTo(methodOn(LibController.class).getBookById(book.getId())).withSelfRel(),
                linkTo(methodOn(LibController.class).getAllBooks()).withRel("all items"));
    }
}
