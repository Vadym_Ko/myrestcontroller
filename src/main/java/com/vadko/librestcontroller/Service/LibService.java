package com.vadko.librestcontroller.Service;

import com.vadko.librestcontroller.DAO.BookRepo;
import com.vadko.librestcontroller.Model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *  Service layer for library controller
 */

@Service
public class LibService {

    private BookRepo repo;

    /**
     * Constructor
     * @param repo object of JPA Repository type
     */
    @Autowired
    public LibService(BookRepo repo) {
        this.repo = repo;
    }

    public List<Book> findAllBooks() {
        return repo.findAll();
    }

    public Book saveBook (Book book) {

       return repo.save(new Book(book.getTitle(),book.getAuthor(),book.getYear(),book.getGenre(),
               book.getDescription(),book.getBookcaseNumber()));
    }


    public Book findBookById(long id) { return repo.findById(id); }


    public List<Book> findBooksByTitle(String title) {
        return repo.findByTitleStartsWithIgnoreCase(title);
    }


    public List<Book> findBooksByAuthor(String author) {
        return repo.findByAuthorStartsWithIgnoreCase(author);
    }


    public List<Book> findBooksByYear(String year) {
        return repo.findByYearStartsWithIgnoreCase(year);
    }


    public List<Book> findBooksByGenre(String genre) {
        return repo.findByGenreStartsWithIgnoreCase(genre);
    }


    public List<Book> findByBookBookcaseNumber(String bookcaseNumber) {
        return repo.findByBookcaseNumberStartsWithIgnoreCase(bookcaseNumber);
    }

    public Book updateBook(Book changedBook, long id){

        Book bookFromDB = repo.findById(id);
        copyFromBookToBook(changedBook, bookFromDB);

        return repo.save(bookFromDB);
    }

    public void deleteBook (long id){
        repo.deleteById(id);
    }

    private void copyFromBookToBook(Book changedBook, Book bookFromDB) {

        bookFromDB.setTitle(changedBook.getTitle());
        bookFromDB.setAuthor(changedBook.getAuthor());
        bookFromDB.setYear(changedBook.getYear());
        bookFromDB.setGenre(changedBook.getGenre());
        bookFromDB.setDescription(changedBook.getDescription());
        bookFromDB.setBookcaseNumber(changedBook.getBookcaseNumber());
    }


}
