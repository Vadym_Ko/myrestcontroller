package com.vadko.librestcontroller.Model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import java.util.Objects;

/**
 * Describes books for the library
 */

@Data
@Entity
public class Book {

    @Id
    @GeneratedValue
    @JsonProperty("id")
    private long id;
    @NotBlank
    private String title;
    @NotBlank
    private String author;
    @NotBlank
    private String year;
    private String genre;
    private String description;
    private String bookcaseNumber;

    public Book(){

    }

    /**
     * Book for library. Id is generated automatically
     * @param title title of the book, couldn't be blank
     * @param author author of the book, couldn't be blank
     * @param year year of the book, couldn't be blank
     * @param genre genre of the book
     * @param description description of the book
     * @param bookcaseNumber bookcase number where the book is put
     */
    public Book(@JsonProperty("title") String title,
                @JsonProperty("author") String author,
                @JsonProperty("year") String year,
                @JsonProperty("genre") String genre,
                @JsonProperty("description") String description,
                @JsonProperty("bookcaseNumber") String bookcaseNumber) {

        this.title = title;
        this.author = author;
        this.year = year;
        this.genre = genre;
        this.description = description;
        this.bookcaseNumber = bookcaseNumber;
    }

}