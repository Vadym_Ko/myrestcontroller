package com.vadko.librestcontroller;

import com.vadko.librestcontroller.DAO.BookRepo;
import com.vadko.librestcontroller.Model.Book;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class MyrestcontrollerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyrestcontrollerApplication.class, args);
    }


}