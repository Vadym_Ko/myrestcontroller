package com.vadko.librestcontroller;


import com.jayway.restassured.RestAssured;
import com.vadko.librestcontroller.DAO.BookRepo;
import com.vadko.librestcontroller.Model.Book;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class LibTest {

    @Autowired
    BookRepo repo;

    @Before
    public void loadData() {

        RestAssured.baseURI = "http://localhost:8080/api/v1/book";

        repo.save(new Book("title1", "author1", "year1",
            "gen1re", "description1", "bnum1"));
        repo.save(new Book("title2", "author2", "year2",
            "gen1re", "description2", "bnum2"));
        repo.save(new Book("title5", "author5", "year5",
            "genre", "description5", "bnum5"));
        repo.save(new Book("title6", "author6", "year6",
            "genre", "description6", "bnum6"));

        }

    @Test
    public void canRetrieveAllBooks() {

        given()
                .when()
                .get()
                .then().statusCode(200).body(containsString("author1"))
          .and()
                .body(containsString("author6"));

    }

    @Test
    public void canRetrieveBookById() {

        given()
                .when()
                .get("/id/1")
                .then().statusCode(200).body(containsString("author1"))
          .and()
                .body(containsString("year1"));
    }

    @Test
    public void canAddANewBook() {
        Book newBook = new Book("title3", "author3", "year3",
                "gen1re", "description3", "bnum3");
        given()
                .contentType("application/json")
                .body(newBook)
                .when()
                .post()
                .then().statusCode(201);
        given()
                .when()
                .get()
                .then().statusCode(200).body(containsString("author3"))
                .and()
                .body(containsString("bnum3"));
    }

    @Test
    public void canUpdateAnExistingBook() {
        Book newBook = new Book("title1", "author1", "year1",
                "gen1re", "description1", "bnum35");
        given()
                .contentType("application/json")
                .body(newBook)
                .when()
                .put("/id/1")
                .then().statusCode(201);
        given()
                .when()
                .get()
                .then().statusCode(200).body(containsString("title1"))
                .and()
                .body(containsString("bnum35"));
    }

    @Test
    public void canDeleteAnExistingBookById() {
        given()
                .pathParam("id", 2)
                .when()
                .delete("/id/{id}")
                .then().statusCode(204);
        given()
                .pathParam("id", 2)
                .when()
                .get("/id/{id}")
                .then().statusCode(500);
    }

    @Test
    public void canFilterBooksByGenre() {

        given()
                .pathParam("genre", "genre")
                .when()
                .get("/genre/{genre}")
                .then().statusCode(200).body(not(containsString("gen1re")));
    }
}