REST controller. v.1.0 (this example is for managing books)

  This program is an example of REST controller based on SPRING BOOT.  It can save, update, 
delete and find books by parameters. Every book must be saved with following fields: 
id (generated field), title, author, year, genre, description, bookcase number (number 
or description of place where it is actually stores). Program takes and sends data in 
JSON format. Also you could get a list of books filtered by title, author, year, genre, 
bookcase number. 
  After start, by default, program listens 8080 port for POST, PUT, DELETE and GET
queries with parameters. Program uses H2 database, and can be simply configured fo using
MySQL, PostgreSQL or another database.

  Map of queries:
  
  {-PATH-} = http://your_domain.com:8080/api/v1/book
  
  POST {-PATH-} + body (JSON format) - to save a new book
  
  GET {-PATH-} - to get a list of all books from library
  
  GET  {-PATH-}/id/"id_parametr" - to get book by specified id
  
  PUT  {-PATH-}/id/"id_parametr" + body (JSON format) - to update a book with specified id
  
  DETETE {-PATH-}/id/"id_parametr" - to delete a book with specified id
  
  GET {-PATH-}/title/"title_parametr" - to get a list of books with specified title
  
  GET {-PATH-}/author/"author_parametr" - to get a list of books with specified author
  
  GET {-PATH-}/year/"year_parametr" - to get a list of books with specified year
  
  GET {-PATH-}/genre/"genre_parametr" - to get a list of books with specified genre
  
  GET {-PATH-}/bn/"bookcase_number_parametr" - to get a list of books with specified bookcase number
  
  JSON format of query:
  {
          "title": "Some Title",
          "author": "Some Author",
          "year": "Year",
          "genre": "Genre",
          "description": "Short Description",
          "bookcaseNumber": "Bkcase number"
  }
  
  

  
 
  
  
 